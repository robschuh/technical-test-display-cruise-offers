
/*
* Reads a json file in the server.
*/
function loadJSON(callback) {
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open('GET', 'files/cruise.json', true); // Replace 'my_data' with the path to your file
  xobj.onreadystatechange = function () {
    console.log(xobj.status);
  if (xobj.readyState == 4 && xobj.status == "200") {
    // Call callback.
    callback(xobj.responseText);
  } else {
   callback(false);
  }

  };
  xobj.send(null);
}

function loadContent() {

// Callback function
loadJSON((response) => {

	// Parse JSON string into object
	var actual_JSON = JSON.parse(response);

	// Inject each cruise DIV inside main cruises DIV.
  console.log(actual_JSON.length);


  if(actual_JSON === false){
      $(".loading").html('');
      $("#status").html('<h4>Please upload the file cruise.json</h4>');
  } else {

  	actual_JSON.forEach((row) => {
  		$("#status").html('');
  		$( "#cruises" ).append(`<div class="cruise">
       		<h2 class="title">${row.title}</h2>
  	    	<ul>
  			   <li >Cruise 1 - 14 nights</li>
  			   <li class="description">${row.description}</li>
  	    	</ul>
     		</div>` );
  	});
  }
});
}

// Load the content on the page.
loadContent();
