<?php

/*
* Uploads a file to the server.
*/
function uploadFile($file){

//var_dump($_FILES['cruise']['name']);
$target_dir = "files/";
$target_file = $target_dir . basename($file["cruise"]["name"]);
$uploadOk = 1;
$fileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
$errorMsg = '';

if($fileType != 'json'){
	$errorMsg = 'File format not supported, please upload a json file';
}

if(empty($errorMsg)){
  	if (move_uploaded_file($file["cruise"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $file["cruise"]["name"]). " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}

else{
   echo $errorMsg;
}

}
