<!DOCTYPE html>
<head>
<title>cruise results</title>
<link href="vendors/bootstrap-4.0.0-dist/css/bootstrap.css" rel="stylesheet">
<link href="css/cruise.css?<?php echo rand(0, 10000); ?>" rel="stylesheet">
<script
  src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
  integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
  crossorigin="anonymous"></script>
<meta charset="UTF-8">
</head>
<body>
<div class="container-fluid content">
<header>
  <div class="row">
    <div class="offset-3 col12">
      <p class="alert">
    	  <?php
    		if(isset($_FILES['cruise'])){
    			require_once('upload.php');
    			uploadFile($_FILES);
    		}
    	   ?>
       </p>
    </div>
  </div>
  <div class="row">
    <div class="offset-3 col6">
       <h3>Results</h3>
    </div>
    <div class="offset-3 col3">
      <form name="uploadFile" method="post" action="index.php" enctype="multipart/form-data">
           <input type="file" name="cruise">
           <input type="submit" value="Upload">
      </form>
    </div>
  </div>
</header>
<article>
  <div class="row">
    <div class="offset-3 col6" id="cruises">
    	<p class="loading">Loading async content...</p>
      <p id="status"></p>
    </div>
  </div>
</article>
<footer>Technical test</footer>
	<script type="text/javascript" src="js/loadAsyncContent.js"></script>
</div>
